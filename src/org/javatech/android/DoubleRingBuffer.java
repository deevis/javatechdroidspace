package org.javatech.android;

public class DoubleRingBuffer {

  private double[] data = null;
  private int readIndex = 0;
  private int writeIndex = 0;
  private int size = 0;
  private int maxSize = 0;
  private Double avg = null;
  
  public DoubleRingBuffer(int maxSize) {
    super();
    this.maxSize = maxSize;
    reset();
  }

  public void addValue(double value) {
    data[writeIndex] = value;
    writeIndex = (writeIndex+1) % maxSize;
    if ( size >= maxSize ) {
      readIndex = (readIndex + 1) % maxSize;
    }
    if ( size < maxSize ) {
      size++;
    }
    this.avg = null;
  }
  
  public void reset() {
    size = 0;
    readIndex = 0;
    writeIndex = 0;
    this.avg = null;
    data = new double[maxSize];
  }
  
  public double[] getValues() {
    double[] values = new double[size];
    for (int i = 0; i < size - 1; i++ ) {
      values[i] = data[ (readIndex+i) % maxSize];
    }
    return values;
  }
  
  
  public double getAverage() {
    if ( this.avg != null ) {
      return this.avg;  // Already computed...
    }
    double avg = 0;
    if ( size == 0 ) {
      return 0;
    }
    for (int i = 0; i < size; i++ ) {
      avg += data[ (readIndex+i) % maxSize];
    }
    this.avg = avg / size;
    return this.avg;
  }

  public double getStdDev() {
    if ( size == 0 ) {
      return 0;
    }
    double avg = getAverage();
    double sum = 0;
    for (int i = 0; i < size; i++ ) {
      sum += Math.pow(data[ (readIndex+i) % maxSize] - avg, 2);
    }
    return Math.sqrt( sum / size );
  }
  
  public double getMinimum() {
    if ( size == 0 ) {
      return 0;
    }
    double minimum = Double.MAX_VALUE;
    for (int i = 0; i < size; i++ ) {
      double val = data[ (readIndex+i) % maxSize];
      if ( val < minimum ) {
        minimum = val;
      }
    }
    return minimum;
  }

  public double getMaximum() {
    if ( size == 0 ) {
      return 0;
    }
    double maximum = Double.MIN_VALUE;
    for (int i = 0; i < size; i++ ) {
      double val = data[ (readIndex+i) % maxSize];
      if ( val > maximum ) {
        maximum = val;
      }
    }
    return maximum;
  }

}
