package org.javatech.android;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

public class FrontBackImageView extends RelativeLayout {

  private ImageView backImage = new ImageView(getContext());
  private ImageView frontImage = new ImageView(getContext());
  private ImageView animationOverlay = new ImageView(getContext());
  
  protected Rect backImagePadding = new Rect(0,0,0,0);
  protected Rect frontImagePadding = new Rect(0,0,0,0);
  protected Rect animationOverlayPadding = new Rect(0,0,0,0);
  
  public FrontBackImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
    frontImage.setScaleType(ScaleType.FIT_XY);
    LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
    frontImage.setLayoutParams(params);

    backImage.setScaleType(ScaleType.FIT_XY);
    backImage.setBackgroundColor(Color.WHITE);
    params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
    params.addRule(CENTER_IN_PARENT);
    backImage.setLayoutParams(params);
    backImage.setScrollBarStyle(GONE);
    
    animationOverlay.setLayoutParams(params);
    animationOverlay.setScaleType(ScaleType.FIT_XY);
    
    this.addView(backImage);
    this.addView(frontImage);
    this.addView(animationOverlay);
  }

  public void setBackImage(Drawable d) {
    setBackImage(d, 255);
  }
  public void setBackImage(Drawable d, int alpha) {
    doImageSet(d, backImage, backImagePadding, alpha);
  }

  public void setFrontImage(Drawable d) {
    setFrontImage(d, 255);
  }
  
  public void setFrontImage(Drawable d, int alpha) {
    doImageSet(d, frontImage, frontImagePadding, alpha);
  }

  public void setAnimationOverlayImage(Drawable d, int alpha) {
    doImageSet(d, animationOverlay, new Rect(0,0,0,0), alpha);
  }

  /** 
   * Slides up the existing image and then replaces with passed Drawable
   */
  public void slideUpAnimationOverlay(int time) {
    Animation a = new TranslateAnimation(0,0,0,-1 * animationOverlay.getHeight());
    a.setDuration(time);
    a.setFillAfter(true);
    animationOverlay.startAnimation(a);
  }

  public void slideDownAnimationOverlay(int time) {
    Animation a = new TranslateAnimation(0,0,-1 * animationOverlay.getHeight(),0);
    a.setDuration(time);
    a.setFillAfter(true);
    animationOverlay.startAnimation(a);
  }
  
  /**
   * Fade away to nothing...
   * 
   * This causes a bug in Android-2.1 where many other drawables of the same resource get changed and corrupted.
   * @param d
   */
  public void fadeToNothingAnimation(Drawable d) {
    Animation a = new ScaleAnimation(1f,0f,1f,0f,Animation.ABSOLUTE,50,Animation.ABSOLUTE,50);
    a.setDuration(1000);
    a.setFillAfter(true);
    doImageSet(d, animationOverlay, animationOverlayPadding, 255);
    animationOverlay.startAnimation(a);
  }
  
  private void doImageSet(Drawable d, ImageView imageView, Rect r, int alpha) {
    if ( d == null ) 
      imageView.setPadding(0,0,0,0);
    else 
      imageView.setPadding(r.left, r.top, r.right, r.left);
    imageView.clearAnimation();
    imageView.setAlpha(alpha);
    imageView.setImageDrawable(d);
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    this.backImage = null;
    this.animationOverlay = null;
    this.frontImage = null;
  }
  
  
}
