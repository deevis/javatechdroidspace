package org.javatech.android;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.http.client.methods.HttpGet;
import org.javatech.android.task.ImageFetcherTask;
import org.javatech.android.task.TaskCallback;
import org.javatech.android.util.ImageUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class JT {

  public static String APP_NAME = "JavatechDroidSpace";
  public static Random _rand = new Random();
  public static Class resourcesClass = null;
  
  public static final int CAMERA_REQUEST = 325236;
  
  private static final String BARCODE_SCANNER_PACKAGE = "com.google.zxing.client.android";

  private static SoundPool mSoundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
  private static Map<Integer,Integer> mSoundPoolMap = new HashMap<Integer,Integer>();
  private static AudioManager mAudioManager = null;
  public static Map<String,Long> _storedWaits = new HashMap<String,Long>();


  public static void shareIt(final Context c, String subject, String text) {
    Intent intent = new Intent(Intent.ACTION_SEND);
    intent.putExtra(Intent.EXTRA_SUBJECT,subject);
    intent.putExtra(Intent.EXTRA_TEXT,text); 
    //intent.setType("application/twitter");
    intent.setType("text/plain");
    try {
      c.startActivity(Intent.createChooser(intent, "Share with..."));
    }
    catch (ActivityNotFoundException e) {
      /* Handle Exception if no suitable apps installed */
      new AlertDialog.Builder(c)
      .setTitle("Get Twitter")
      .setMessage("No twitter Application not found. Goto market and install one now?")
      .setIcon(R.drawable.icon)
      .setNegativeButton("No", null)
                      .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int whichButton) {
                     intentMarket(c, "market://search?q=twitter");
                   }
               })
      .show();
    }

  }
  
  public static String getSdCardStorage(Context c) {
	  return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separatorChar + "Android/data/" + c.getPackageName();
  }
  
  public static void intentTakePicture(Activity c, String fileName) {
		// Can be complicated - see: http://stackoverflow.com/questions/1910608/android-action-image-capture-intent
		String storageState = Environment.getExternalStorageState();
      if(storageState.equals(Environment.MEDIA_MOUNTED)) {

          String path = ImageUtils.getTempImagePath(c, fileName);
          File _photoFile = new File(path);
          try {
              if(_photoFile.exists() == false) {
                  _photoFile.getParentFile().mkdirs();
                  _photoFile.createNewFile();
              }

          } catch (IOException e) {
              Log.e(JT.APP_NAME, "Could not create file.", e);
          }
          Log.i(JT.APP_NAME, path);

          Uri _fileUri = Uri.fromFile(_photoFile);
          Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE );
          intent.putExtra( MediaStore.EXTRA_OUTPUT, _fileUri);
          c.startActivityForResult(intent, CAMERA_REQUEST);
      }   else {
          new AlertDialog.Builder(c)
          .setMessage("External Storeage (SD Card) is required.\n\nCurrent state: " + storageState)
          .setCancelable(true).create().show();
      }
  }
  public static void intentMarket(Context c, String url) {
    Intent i = new Intent(Intent.ACTION_VIEW);
    Uri u = Uri.parse(url);
    i.setData(u);
    try {
      c.startActivity(i);
    } catch (ActivityNotFoundException e) {
      Toast.makeText(c, "Market not found.", Toast.LENGTH_SHORT).show();
    }
  }

  public static void intentEmail(Context c, String[] recipients, String subject, String text) {
	    Intent i = new Intent(Intent.ACTION_SEND); 
	    i.setType("text/plain"); 
	    i.putExtra(Intent.EXTRA_EMAIL  , recipients); 
	    i.putExtra(Intent.EXTRA_SUBJECT, subject ); 
	    i.putExtra(Intent.EXTRA_TEXT   , text); 
	    try { 
	        c.startActivity(Intent.createChooser(i, "Send mail...")); 
	    } catch (android.content.ActivityNotFoundException ex) { 
	        Toast.makeText(c, "There are no email clients installed.", Toast.LENGTH_SHORT).show(); 
	    }       
	  }
	  
	  public static void intentSms(Context c, String phones, String message) {
	    Log.d(JT.APP_NAME, "SMS: " + phones );
	    Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+phones));
	    sendIntent.putExtra("sms_body", message); 
	    c.startActivity(sendIntent);
	  }
	  

	  
	  public static void intentDialer(Context c, String phoneNumber) {
	    Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber)); 
	    c.startActivity(i);

	  }
	  
	  /**
	   * REQUIRES:
	   * 	<uses-permission android:name="android.permission.CALL_PHONE"></uses-permission>
	   * 
	   * @param c
	   * @param phoneNumber
	   */
	  public static void intentCall(Context c, String phoneNumber) {
	    Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber)); 
	    c.startActivity(i);

	  }
	  

	  public static void intentMap(Context c, CharSequence address) {
	    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="+address));
	    c.startActivity(i); 
	  }

	  public static void intentNavigation(Context c, CharSequence address) {
	    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q="+address));
	    c.startActivity(i); 
	  }

	  /**
	   * 
	   * http://code.google.com/p/zxing/source/browse/trunk/android/src/com/google/zxing/client/android/Intents.java
	   * http://code.google.com/p/zxing/source/browse/trunk/android-integration/src/com/google/zxing/integration/android/IntentIntegrator.java
	   * 
	   * @param a
	   * @param requestCode
	   * @param scanMode  "QR_CODE_MODE" "PRODUCT_MODE"
	   */
	  public static void intentBarcodeScanner(Activity a, int requestCode, String scanMode) {
	    Intent intent = new Intent(BARCODE_SCANNER_PACKAGE + ".SCAN");
	    intent.setPackage(BARCODE_SCANNER_PACKAGE);
	    intent.putExtra("SCAN_MODE", scanMode);
	    intent.addCategory(Intent.CATEGORY_DEFAULT);

	    try {
	      a.startActivityForResult(intent, requestCode);
	    } catch ( ActivityNotFoundException e) {
	      showBarcodeScannerDownloadDialog(a);
	    }
	  }
  
	  /**
	   * Show a dialog to request automatic download of Barcode Scanner application 
	   * 
	   * http://code.google.com/p/zxing/source/browse/trunk/android-integration/src/com/google/zxing/integration/android/IntentIntegrator.java
	   * 
	   * @param activity
	   * @return
	   */
	  private static AlertDialog showBarcodeScannerDownloadDialog(final Activity activity) {
	    AlertDialog.Builder downloadDialog = new AlertDialog.Builder(activity);
	    downloadDialog.setTitle("Install Barcode Scanner?");
	    downloadDialog.setMessage("The Barcode Scanner is not currently installed.  Would you like to automatically install it now?");
	    downloadDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	    public void onClick(DialogInterface dialogInterface, int i) {
	      Uri uri = Uri.parse("market://search?q=pname:" + BARCODE_SCANNER_PACKAGE);
	      Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	      activity.startActivity(intent);
	    }
	    });
	    downloadDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
	    public void onClick(DialogInterface dialogInterface, int i) {}
	    });
	    return downloadDialog.show();
	  }
  
  public static void playSoundWithCallback(final Context c, final int soundId, final long millisToWaitBefore, final long millisToWaitAfter, final Runnable callback) {
    new Thread() {
      public void run() {
        JT.waitMillis(millisToWaitBefore);
        JT.playSound( c, soundId );
        JT.waitMillis(millisToWaitAfter);
        callback.run();
      }
    }.start();
  }

  public static void waitMillis(long millis) {
    try {
      Thread.sleep(millis);
    } catch ( InterruptedException e ) {
      Log.e(APP_NAME, e.getMessage(), e );
    }
  }

  public static synchronized void playSound(Context c, int soundId) {
    if ( mAudioManager == null ) {
      // System services not available before onCreate()
      mAudioManager = (AudioManager)c.getSystemService(Context.AUDIO_SERVICE);
    }
    Integer pooledId = mSoundPoolMap.get(soundId);
    if ( pooledId == null ) {
      JT.cacheSound(c,soundId);
    }
    float streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    streamVolume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    mSoundPool.play(pooledId, streamVolume, streamVolume, 1, 0, 1f);
  }

  public static synchronized int cacheSound(Context c, int soundId) {
    Integer pooledId = mSoundPoolMap.get(soundId);
    if ( pooledId != null ) 
      return pooledId;
    pooledId = mSoundPool.load(c, soundId, 0);
    mSoundPoolMap.put(soundId, pooledId);
    return pooledId;
  }

  public static synchronized void storeWaitMillis(String key, long millisToWait) {
    _storedWaits.put(key, millisToWait);
  }

  public static synchronized long getStoredWaitMillis(String key) {
    Long w = _storedWaits.get(key);
    _storedWaits.put(key, null);
    return (w==null)?0:w;
  }

  public static int findRawResourceByName(String name) {
    try {
      Field field = R.raw.class.getField(name);
      int resourceId = ((Integer)field.get(null)).intValue();
      return resourceId;
    } catch ( Exception e ) {
      return -1;
    }
  }

  public static int findDrawableResourceByName(String name) {
    try {
      Log.d(APP_NAME, "Looking up R.drawable." + name );
      Field field = R.drawable.class.getField(name);
      int resourceId = ((Integer)field.get(null)).intValue();
      Log.d(APP_NAME, "Looking up R.drawable." + name + " returned ["+ resourceId +"]");
      return resourceId;
    } catch ( Exception e ) {
      Log.w(APP_NAME, "Error attempting to access R.drawable." + name );
      return -1;
    }
  }

  public static Object findResourceByName(View v, String name) {
    try {
      Field field = R.id.class.getField(name);
      int resourceId = ((Integer)field.get(null)).intValue();
      return v.findViewById(resourceId);
    } catch ( Exception e ) {
      Log.e(APP_NAME, "Error attempting to access R.id." + name, e );
      return null;
    }
  
  }

  public static void loadDrawableFromUri(Context c, Uri imageUri, TaskCallback<Bitmap> callback) {
    Log.d(JT.APP_NAME, "Attempting to load background image:["+ imageUri.toString() +"]");
//    if ( "content".equals(imageUri.getScheme())) {
      try {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(c.getContentResolver(), imageUri);
        callback.run(bitmap);
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
//      InputStream is = null;
//      try {
//        is = c.getContentResolver().openInputStream(imageUri);
//        Bitmap backgroundBitmap = BitmapFactory.decodeStream(is);
//        BitmapDrawable bd = BitmapDrawable
//      } catch ( Exception e ) {
//        Log.e(JT.APP_NAME, "Error attempting to load background image from Uri", e);
//      } finally {
//        try { is.close();} catch (Exception e) {}
//      }
//    } else if ( imageUri.getScheme().startsWith("http")) {
//      ImageFetcherTask ift = new ImageFetcherTask(c, callback);
//      ift.execute(new HttpGet(imageUri.toString()));
//    } else {
//      Toast.makeText(c, "Unknown Uri scheme: " + imageUri.getScheme(), Toast.LENGTH_SHORT).show();
//      Log.e(JT.APP_NAME, "Unknown Uri scheme: " + imageUri.getScheme() );
//    }
    
  }
  /**
   * Calling getPointOffset(50,150) would give yield a result in the range of [-200,-50] or [50,200]
   * 
   * @return
   */
  public static float getPointOffset(float min, float width) {
    float retVal = _rand.nextFloat() * width + min;
    if ( _rand.nextBoolean()) 
      retVal *= -1.0f;
    //Log.d(JT.APP_NAME, "offset: " + retVal );
    return retVal;
  }

}
