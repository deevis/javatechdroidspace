package org.javatech.android;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Android Tabs are less than perfect
 * 
 * Here's one way to give a bit more graphic freedom...
 * 
 * @author Darren Hicks
 *
 */
public class MultiTabButtonImageView extends ImageView {

  public static final int NO_BUTTON_PLACE_HOLDER = Integer.MIN_VALUE;
  
  private int width, height = 0;
  private int activeIndex = 0;
  private int[] imageResources;
  private Runnable[] clickHandlers;
  
  public MultiTabButtonImageView(Context context) {
    super(context);
  }

  public MultiTabButtonImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
    setOnTouchListener(new OnTouchListener() {

      @Override
      public boolean onTouch(View arg0, MotionEvent arg1) {
        if ( arg1.getAction() != MotionEvent.ACTION_DOWN ) {
          return false;
        }
        if ( imageResources == null || imageResources.length == 0 ) return false;
        int buttonSize = MultiTabButtonImageView.this.width / imageResources.length;
        activeIndex = ((int)arg1.getX())/buttonSize;
        updateImage();
        if ( clickHandlers != null && clickHandlers.length >= activeIndex ) {
          clickHandlers[activeIndex].run();
        }
        return false;
      }
    });
  }

  
  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);
    this.width = w;
    this.height = h;
  }

  public void setImageResources(int[] imageResources) {
    this.imageResources = imageResources;
  }

  public void setClickHandlers(Runnable[] clickHandlers) {
    this.clickHandlers = clickHandlers;
  }

  public int getActiveIndex() {
    return activeIndex;
  }
  
  public void setActiveIndex(int index) {
    activeIndex = index;
    updateImage();
  }
  
  private void updateImage() {
    MultiTabButtonImageView.this.setImageResource(imageResources[activeIndex]);
  }
  

}
