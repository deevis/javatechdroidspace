package org.javatech.android;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.client.methods.HttpGet;
import org.javatech.android.task.ImageFetcherTask;
import org.javatech.android.task.TaskCallback;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageView;

public class RemoteGalleryActivity extends Activity {
  private String[] mUrls;
  private Set<String> urlsDownloading = new HashSet<String>(128);

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle icicle) {
    super.onCreate(icicle);
    setContentView(R.layout.gallery);
    mUrls = getIntent().getStringArrayExtra("urls");
    ((GridView) findViewById(R.id.gridview)).setAdapter(new RemoteImageAdapter(this, mUrls));
  }

  public class RemoteImageAdapter extends BaseAdapter {
    private Context mContext;
    private String[] imageUrls = null;
    private int clearCount;

    public RemoteImageAdapter(Context c, String[] urls) {
      this.mContext = c;
      this.imageUrls = urls;
    }

    public int getCount() {
      return this.imageUrls.length;
    }

    public String getItem(int position) {
      return imageUrls[position];
    }

    public long getItemId(int position) {
      return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
      ImageView i = null;
      if (convertView == null) {  // if it's not recycled, initialize some attributes
        i = new ImageView(mContext);
        i.setLayoutParams(new GridView.LayoutParams(110, 110));
        i.setScaleType(ImageView.ScaleType.CENTER_CROP);
        i.setPadding(4, 4, 4, 4);
    } else {
        i = (ImageView) convertView;
    }

      String url = getItem(position);
      Drawable d = ImageFetcherTask.lookupDrawable(url);
      if ( d != null ) {
        i.setImageDrawable(d) ;
      } else {
        i.setImageDrawable(null);
        lazyLoadImage(position, url);
      }
      return i;
      
//      try {
//        /* Open a new URL and get the InputStream to load data from it. */
//        URL aURL = new URL(imageUrls[position]);
//        URLConnection conn = aURL.openConnection();
//        conn.connect();
//        InputStream is = conn.getInputStream();
//        /* Buffered is always good for a performance plus. */
//        BufferedInputStream bis = new BufferedInputStream(is);
//        /* Decode url-data to a bitmap. */
//        Bitmap bm = BitmapFactory.decodeStream(bis);
//        bis.close();
//        is.close();
//        /* Apply the Bitmap to the ImageView that will be returned. */
//        i.setImageBitmap(bm);
//      } catch (IOException e) {
//        i.setImageResource(R.drawable.ic_cancel);
//        Log.e(JT.APP_NAME, "Remote Image Exception", e);
//      }
//
//      /* Image should be scaled as width/height are set. */
//      i.setScaleType(ImageView.ScaleType.FIT_CENTER);
//      /* Set the Width/Height of the ImageView. */
//      i.setLayoutParams(new GridView.LayoutParams(85, 85));
//      i.setScaleType(ImageView.ScaleType.CENTER_CROP);
//      i.setPadding(8, 8, 8, 8);
//      return i;
//    }

  }
    
    private void lazyLoadImage(final int position, final String url) {
//      final int cc = clearCount;
//      final String thumbUrl = si.getAuthorThumbnail();
//      if ( thumbUrl == null || thumbUrl.trim().length() == 0 || !downloadImages ) 
//        return;
//      if ( thumbUrl == null && si instanceof MediaItem ) {
//        thumbUrl = ((MediaItem)si).getDataThumbnail();
//      }
      // downloadImages may be false if flinging is occurring
      if ( urlsDownloading.contains(url) || ImageFetcherTask.lookupDrawable(url) != null) {
//        Log.d(JT.APP_NAME, thumbUrl + " ALREADY DOWNLOADING " + urlsDownloading.size());
        return;  // Already downloading this
      }
      urlsDownloading.add(url);    // Don't download this again - removed by callback to setRetrievedImage
      ImageFetcherTask lazyLoader = new ImageFetcherTask(RemoteGalleryActivity.this, new TaskCallback<Drawable>() {
        @Override
        public void run(Drawable t) {
          if ( t != ImageFetcherTask.NO_RESULT) {
            setRetrievedImage(t, url, clearCount);
          }
        }
        
      });
      try {
        lazyLoader.setResizeParams(100,100);
        lazyLoader.execute(new HttpGet(new URI( url )) );
      } catch (URISyntaxException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    public void setRetrievedImage(Drawable t, String url, int clearCount) {
      if ( this.clearCount == clearCount ) {
        urlsDownloading.remove(url);
        RemoteImageAdapter.this.notifyDataSetChanged();
      }
    }

//    @Override
//    public synchronized void clear() {
//      clearCount++;   // Make sure we increment this to make sure old images aren't used as current results...
//      super.clear();
//      urlsDownloading.clear();
//    }
  }
}
