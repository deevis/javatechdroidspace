package org.javatech.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;

public abstract class SplashActivity extends Activity {

  protected boolean _active = true;
  
//  private static int[] splashScreens = new int[] { R.layout.splash, R.layout.splash2 };
//  private static long[] splashPauses = new long[] { 1800, 2300 }; // {200,300}; // 
  private SplashConfig[] splashConfig = null;
  private static int currentScreen = 0;
  
  Thread splashThread = getSplashThread();

@Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    splashConfig = getSplashConfig();
    if ( currentScreen >= splashConfig.length ) {
      currentScreen = 0;
//      return;
    }
    setContentView(splashConfig[currentScreen].layoutId);
    
    splashThread = getSplashThread();
    if ( !splashThread.isAlive() )
      splashThread.start();
    splashThread = null;
  }

  private Thread getSplashThread() {
    return new Thread() {
      @Override
      public void run() {
          try {
            JT.waitMillis(splashConfig[currentScreen].millisToShow);
          } finally {
            currentScreen++;
              if ( currentScreen >= splashConfig.length ) {
                finish();
                try {
                  startActivity(getMainApplicationIntent());
                } catch ( ActivityNotFoundException e) {
                  e.printStackTrace();
                }
              } else {
                finish();
                startActivity(new Intent(getApplicationContext(), getActivityClass()));
              }
          }
      }
  };
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    splashThread = null;
    // Trying to prevent OutOfMemoryException: bitmap size exceeds VM budget 
    // http://code.google.com/p/android/issues/detail?id=8488
    System.gc();  
  }
  
  protected abstract Intent getMainApplicationIntent();
  
  protected Class<?> getActivityClass() {
    return this.getClass();
  }
  
  protected SplashConfig[] getSplashConfig() {
    return new SplashConfig[] {
      new SplashConfig(R.layout.splash,1800),
      new SplashConfig(R.layout.splash2,2300),
    };
  }
}

