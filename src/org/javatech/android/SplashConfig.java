package org.javatech.android;

public class SplashConfig {
    public int layoutId;
    public long millisToShow;
    
    public SplashConfig(int layoutId, long millisToShow) {
      this.layoutId = layoutId;
      this.millisToShow = millisToShow;
    }
    
}
