package org.javatech.android;

public class TimeUtils {

	  private static final long MILLIS_IN_A_SECOND = 1000L;
	  private static final long MILLIS_IN_A_MINUTE = 60L * MILLIS_IN_A_SECOND;
	  private static final long MILLIS_IN_A_HOUR = 60L * MILLIS_IN_A_MINUTE;
	  private static final long MILLIS_IN_A_DAY = 24L * MILLIS_IN_A_HOUR;
	  private static final long MILLIS_IN_A_WEEK = 7L * MILLIS_IN_A_DAY;
	  private static final long MILLIS_IN_A_MONTH = 30L * MILLIS_IN_A_DAY;

	/**
	 * Builds a friendly message based on the relative time between the current system time and the value passed in
	 * 
	 * @param millisTimeStamp
	 * @return
	 */
	public static String buildFriendlyTimeFromNow( long millisTimeStamp ) {
		long diff = System.currentTimeMillis() - millisTimeStamp;	
		String direction = " ago";
		if ( diff < 0 ) {
			direction = " from now";
			diff *= -1;		// Normalize for comparisons below
		}
		String periodType = null;
		int amount = -1;
		if ( diff > MILLIS_IN_A_MONTH ) {
			amount = (int)(diff/MILLIS_IN_A_MONTH);
			periodType=" Month";
		} else if ( diff > MILLIS_IN_A_WEEK ) {
			amount = (int)(diff/MILLIS_IN_A_WEEK);
			periodType=" Week";
		} else if ( diff > MILLIS_IN_A_DAY ) {
			amount = (int)(diff/MILLIS_IN_A_DAY);
			periodType=" Day";
		} else if ( diff > MILLIS_IN_A_HOUR ) {
			amount = (int)(diff/MILLIS_IN_A_HOUR);
			periodType=" Hour";
		} else if ( diff > MILLIS_IN_A_MINUTE ) {
			amount = (int)(diff/MILLIS_IN_A_MINUTE);
			periodType=" Minute";
		} else {
			amount = (int)(diff/MILLIS_IN_A_SECOND);
			periodType=" Second";
		}
		if ( amount < 1 ) amount = 1;
		if ( amount > 1) {
			periodType += "s";
		}
		return amount + periodType + direction;
	}
	
	
	

}
