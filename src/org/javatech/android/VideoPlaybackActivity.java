package org.javatech.android;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlaybackActivity extends Activity {

  public static final String VIDEO_URI = "VIDEO_URI";
  
  VideoView videoView;
  MediaController controller;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.video_playback);
    videoView = (VideoView)findViewById(R.id.VideoView);
    String videoUri = getIntent().getStringExtra(VIDEO_URI);
    videoView.setVideoURI( Uri.parse( videoUri ));
    controller = new MediaController(this);
    videoView.setMediaController(controller);
    videoView.start();
    
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    videoView.stopPlayback();
  }
  

  
}
