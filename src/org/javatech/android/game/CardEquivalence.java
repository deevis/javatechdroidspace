package org.javatech.android.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.content.Context;

public class CardEquivalence {

  public static Map<String,CardEquivalence> variantsMap = new HashMap<String,CardEquivalence>();

  public String id;
  public List<Variant> variants = new ArrayList<Variant>();
  public String imagePrefix = "";
  public String[] banks = null;
  public Map<String,Variant> bankVariants = new HashMap<String,Variant>();
  
  public CardEquivalence(Context c, String id, String[] banks, String imagePrefix) {
    this.id = id;
    this.banks = banks;
    this.imagePrefix=imagePrefix;
    CardEquivalence.variantsMap.put(id, this);
    addImageVariant(c, banks);
  }

  public CardEquivalence(Context c, String id, String[] banks) {
    this(c, id,banks,"");
  }

  public CardEquivalence addTextVariant(String...strings) {
    for ( String s : strings ) {
      variants.add( new TextVariant(this.id, s));
    }
    return this;
  }
  
  public CardEquivalence addImageVariant(int... resourceIds) {
    for ( int rId : resourceIds ) {
      variants.add( new ImageVariant(id, rId));
    }
    return this;
  }

  private CardEquivalence addImageVariant(Context c, String... banks) {
    for ( String bank : banks ) {
      Variant v = new ImageVariant(c, id, bank, imagePrefix);
      bankVariants.put(bank, v);
      variants.add( v );
    }
    return this;
  }

  private static Random _rand = new Random();
  
  public Variant[] getVariantPair(String banksString) {
    String[] banks = banksString.split(",");
    if ( banks.length == 2 ) {
      Variant[] r = new Variant[]{bankVariants.get(banks[0]), bankVariants.get(banks[1])};
      return r;
    }
    if ( banks.length == 1 ) {
      Variant[] r = new Variant[]{bankVariants.get(banks[0]), bankVariants.get(banks[0])};
      return r;
    }
    if ( banks.length > 2 ) {
      Variant[] r = new Variant[2];
      r[0] = bankVariants.get(banks[ _rand.nextInt(banks.length)]);
      do {
        r[1] = bankVariants.get(banks[ _rand.nextInt(banks.length)]);
      } while ( r[0].bank.equals(r[1].bank));
      return r;
    }
    return null;
  }
  
  public Variant[] getVariantPair() {
    if (variants == null || variants.size() == 0 ) {
      throw new RuntimeException("No variants to workwith for id["+id+"]");
    }
    Variant[] v = new Variant[2];
    if ( variants.size() == 1) {
      v[0] = variants.get(0);
      v[1] = variants.get(0);
    } else {
      List<Variant> temp = new ArrayList<Variant>(variants);
      Collections.shuffle(temp);
      v[0] = temp.remove(0);
      v[1] = temp.remove(0);
    }
    return v;
    
  }
}
