package org.javatech.android.game;

import org.javatech.android.FrontBackImageView;
import org.javatech.android.JT;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

public abstract class GameCard extends FrontBackImageView {

  
  public Drawable mCardDrawable = null;
  
  public String mShownText = null;
  public String mHiddenText = null;
  public String mMatchedText = null;
  public Variant mVariant = null;
  
  public String mText = null;
  public boolean hideCards = true;
  
  public boolean cardShown = false;
  public boolean matched = false;

  
  public GameCard(Context context, AttributeSet attrs) {
    super(context, attrs);
    this.backImagePadding = new Rect(10,10,10,10);
    this.animationOverlayPadding = new Rect(0,0,0,0);
    this.setPadding(5, 5, 5, 5);
    configImages(context);
  }

  private void configImages(Context context) {
//    mEmptyPaneDrawable = context.getResources().getDrawable(R.drawable.empty_pane);
//    mHiddenPaneDrawable = context.getResources().getDrawable(R.drawable.hidden_pane);
//    mWhitePaneDrawable = context.getResources().getDrawable(R.drawable.white_pane);
//    mGlassPaneDrawable = context.getResources().getDrawable(R.drawable.glass_pane);
  }

  public void init(Variant v) {
    this.mVariant = v;
    this.matched = false;
    this.cardShown = false;
    if ( v instanceof TextVariant ) {
      this.mShownText = ((TextVariant)v).text;
    } else if ( v instanceof ImageVariant ) {
      this.mCardDrawable = ((ImageVariant)v).getDrawable(this.getContext());
      //Bitmap bm = Bitmap.createBitmap(mCardDrawable.getIntrinsicWidth()+10, mCardDrawable.getIntrinsicHeight()+10, Config.ARGB_8888);
    }
    if ( hideCards ) {
      setAnimationOverlayImage(getContext().getResources().getDrawable(getHiddenPaneResourceId()), 255);
      setFrontImage(getContext().getResources().getDrawable(getEmptyResourceId()));
      setBackImage(mCardDrawable);
    } else {
      setAnimationOverlayImage(getContext().getResources().getDrawable(getTransparentOverlayId()), 128);
      setFrontImage(getContext().getResources().getDrawable(getEmptyResourceId()));
      setBackImage(mCardDrawable);
    }
    this.hideCard();
  }
  
  
  public void hideCard() {
    if ( cardShown || matched )
      slideDownAnimationOverlay(700);
    cardShown = false;
  }

  public boolean isRevealing() {
    return mVariant.isRevealing();
  }
  
  public void showCard() {
    mVariant.setRevealing(true);
    if ( mVariant.hasSound() ) {
      JT.playSound(getContext(), getRevealSoundResourceId());
      JT.playSoundWithCallback(getContext(), mVariant.getSoundResourceId(), 500, 500, 
          new Runnable() { 
            public void run() { 
              mVariant.setRevealing(false); 
            }
          });
    } else {
      JT.playSoundWithCallback(getContext(), getRevealSoundResourceId(), 0, 700, 
          new Runnable() { 
            public void run() { 
              mVariant.setRevealing(false); 
            }
          });
    }
    cardShown = true;
    if ( mShownText != null ) {
      writeText(mShownText);
    } else {
//      setBackImage(mCardDrawable,255);
//      setFrontImage(mEmptyPaneDrawable);
      slideUpAnimationOverlay(500);
    }
  }
  
  public boolean isMatched() {
    return this.matched;
  }
  
  public void matched() {
    cardShown = false;
    this.setClickable(false);
    matched = true;
    setFrontImage(getContext().getResources().getDrawable(getBlankResourceId()));
    setBackImage(null);
    this.animationOverlayPadding = new Rect(10,10,10,10);
    //fadeToNothingAnimation(mCardDrawable);
    JT.waitMillis(100);
  }
  
  private void writeText(String t) {
    mText = t;
    this.postInvalidate();
  }
  
  public boolean checkForMatch( GameCard gc ) {
    return this.mVariant.equals( gc.mVariant );
  }
  
  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    this.mCardDrawable.setCallback(null);
    this.mCardDrawable = null;
  }
  
  public abstract int getHiddenPaneResourceId();
  public abstract int getBlankResourceId();
  public abstract int getEmptyResourceId();
  public abstract int getTransparentOverlayId();
  
  public abstract int getRevealSoundResourceId();
}
