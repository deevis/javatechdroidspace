package org.javatech.android.game;


import org.javatech.android.JT;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class ImageVariant extends Variant {

  private int resourceId;
  private String imagePrefix = "";
  
  public ImageVariant(String id, int resourceId) {
    super(id);
    this.resourceId = resourceId;
  }

  public ImageVariant(Context c, String id, String bank) {
    this(c, id, bank, "");
  }
  
  public ImageVariant(Context c, String id, String bank, String imagePrefix) {
    super(id);
    this.imagePrefix = imagePrefix;
    String name = imagePrefix + id + bank;
    this.resourceId = JT.findDrawableResourceByName(name);
    this.soundResourceId = JT.findRawResourceByName(id+bank);
    if ( soundResourceId > -1 ) {
      JT.cacheSound( c, soundResourceId );
    }
    this.bank = bank;
  }

  public Drawable getDrawable(Context c) {
    return c.getResources().getDrawable(resourceId);
  }

  public String getIdWithBank() {
    return id + bank;
  }
}
