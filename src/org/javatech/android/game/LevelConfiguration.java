package org.javatech.android.game;

import java.util.HashMap;
import java.util.Map;

import org.javatech.android.JT;

import android.content.Context;
import android.util.Log;

public class LevelConfiguration {
  
  public String lookupKey = null;
  public String[] cards;
  public String imgPrefix = "";
  public String[] banks = null;
  public String[] rounds = null;
  private boolean initialized = false;
  
  private static Map<String,LevelConfiguration> configLookup = new HashMap<String,LevelConfiguration>();
  
  public LevelConfiguration(String lookupKey, String[] cards, String imgPrefix, String[] banks, String[] rounds) {
    Log.d(JT.APP_NAME, "Registering LevelConfiguration ["+ lookupKey +"]");
    this.lookupKey = lookupKey;
    this.cards = cards;
    this.imgPrefix = imgPrefix;
    this.banks = banks;
    this.rounds = rounds;
    configLookup.put(lookupKey, this);
  }

  public static synchronized LevelConfiguration find(Context c, String lookupKey) {
    LevelConfiguration lc = configLookup.get(lookupKey);
    Log.d(JT.APP_NAME, "LevelConfiguration.find('" + lookupKey + "')");
    if ( !lc.initialized ) {
      lc.init(c);
    }
    return lc;
  }
  
  private void init(Context c) {
    this.initialized = true;
    for ( String card : cards ) {
      new CardEquivalence(c, card, banks,imgPrefix);
    }
  }
}
