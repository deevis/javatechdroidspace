package org.javatech.android.game;


public class TextVariant extends Variant {

  
  public String text;
  
  public TextVariant(String id, String text) {
    super(id);
    this.text = text;
  }
}
