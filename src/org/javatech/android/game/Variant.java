package org.javatech.android.game;

public class Variant {

  public String id;
  protected int soundResourceId = -1;
  private boolean revealing = false;
  
  public String bank = null;   // Use this in looking up image/sound files
  
  public Variant(String id) { 
    this.id = id;
  }
  
  public void setRevealing(boolean r) {
    this.revealing = r;
  }
  
  public boolean isRevealing() {
    return revealing;
  }
  
  public boolean hasSound() {
    return soundResourceId >= 0;
  }
  
  public int getSoundResourceId() {
    return this.soundResourceId;
  }
  
//  public void playSound() {
//    playSound(0);
//  }
//  
//  public void playSound(final long delayMillis) {
//    if ( soundResourceId > -1 ) {
//      C.storeWaitMillis("cardSound", 1000);
//      if ( delayMillis == 0 ) {
//        C.playSound( C.context, this.soundResourceId );
//        revealing=false;
//      } else {
//        new Thread() {
//          public void run() {
//            C.waitMillis(delayMillis);
//            C.playSound( C.context, soundResourceId );
//            revealing=false;
//          }
//        }.start();
//      }
//    } else {
//      C.storeWaitMillis("cardSound", 0);
//      revealing=false;
//    }
//  }
  

  @Override
  public boolean equals(Object o) {
    if ( !(o instanceof Variant)) 
      return false;
    Variant v = (Variant)o;
    if ( v == null || id == null) 
      return false;
    return id.equals(v.id);
  }
}
