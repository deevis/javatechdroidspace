package org.javatech.android.media;


import org.javatech.android.R;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.ToggleButton;

public class RecordingActivity extends Activity implements SurfaceHolder.Callback {

  //MediaRecorder recorder;
  //SurfaceHolder holder;
  //boolean recording = false;
  
  //String recordedFileName = null;
  
  private VideoResourceCapture vrc = null;
  
@Override
public void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  requestWindowFeature(Window.FEATURE_NO_TITLE);
  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
          WindowManager.LayoutParams.FLAG_FULLSCREEN);
  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

  setContentView(R.layout.recording);
  SurfaceView cameraView = (SurfaceView) findViewById(R.id.CameraView);
  cameraView.setClickable(true);
  SurfaceHolder holder = cameraView.getHolder();
  holder.addCallback(this);
  holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
}


public void btnToggleRecording(View v) {
  ToggleButton tb = (ToggleButton)v;
  if (!tb.isChecked()) {
      vrc.stop();
      Intent data = new Intent();
      data.putExtra("recordedFileName", vrc.getVideoResource());
      setResult(RESULT_OK, data);
      // Let's initRecorder so we can record again
      vrc.initRecorder();
      try {
        vrc.prepareRecorder();
      } catch ( Exception e ) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        finish();
      }
  } else {
      vrc.start();
  }
}

public void surfaceCreated(SurfaceHolder holder) {
  vrc = new VideoResourceCapture(new MediaRecorder(), holder);
  vrc.initRecorder();
  try {
    vrc.prepareRecorder();
  } catch ( Exception e ) {
    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    finish();
  }
}

public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {}

public void surfaceDestroyed(SurfaceHolder holder) {
  if ( vrc != null ) {
    vrc.destroy();
  }
  finish();
}

}
