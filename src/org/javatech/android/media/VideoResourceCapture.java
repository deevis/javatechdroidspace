package org.javatech.android.media;

import java.io.IOException;

import org.javatech.android.model.VideoResource;

import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.view.SurfaceHolder;

public class VideoResourceCapture {

  public static final String FILENAME_PATTERN = "/sdcard/hands_up_video_";

  private MediaRecorder recorder;       // Initialized in initRecorder
  private SurfaceHolder holder;
  private String recordedFileName;
  private boolean recording = false;
  
  public VideoResourceCapture(MediaRecorder recorder, SurfaceHolder holder) {
    this.recorder = recorder;
    this.holder = holder;
  }
  
  public VideoResource getVideoResource() {
    VideoResource vr = new VideoResource();
    vr.setUri(recordedFileName);
    return vr;
  }
  
  
  public void initRecorder() {
    recordedFileName = FILENAME_PATTERN + System.currentTimeMillis() + ".mp4";
    recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
    recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

    CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_LOW);  // Requires API Level 8 - 2.2
    recorder.setProfile(cpHigh);
    recorder.setOutputFile(recordedFileName);
    recorder.setMaxDuration(50000); // 50 seconds
    recorder.setMaxFileSize(5000000); // Approximately 5 megabytes
  }

  
  public void prepareRecorder() throws Exception {
    recorder.setPreviewDisplay(holder.getSurface());

    try {
        recorder.prepare();
    } catch (IllegalStateException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
  }

  public void stop() {
    recorder.stop();
    recording = false;
  }
  
  public void start() {
    recording = true;
    recorder.start();
  }
  
  public void destroy() {
    if (recording) {
      recorder.stop();
      recording = false;
    }
    recorder.release();
  }
}
