package org.javatech.android.model;

import java.util.Date;
import java.util.Map;

public class DynamicModel {

  // Simple - values contain text
  private Map<String,String> textProperties;
  
  // Simple - values are integers
  private Map<String,Integer> intProperties;
  
  // Simple - values are dates
  private Map<String,Date> dateProperties;
  
  // Values are URI strings that identify audio streams
  private Map<String,AudioResource> audioResources;
  
  // Values are URI strings that identify images
  private Map<String,ImageResource> imageResources;
  
  // Values are URI strings that identify videos
  private Map<String,VideoResource> videoResources;

  
  public Map<String, String> getTextProperties() {
    return textProperties;
  }

  public void setTextProperties(Map<String, String> textProperties) {
    this.textProperties = textProperties;
  }

  public Map<String, Integer> getIntProperties() {
    return intProperties;
  }

  public void setIntProperties(Map<String, Integer> intProperties) {
    this.intProperties = intProperties;
  }

  public Map<String, Date> getDateProperties() {
    return dateProperties;
  }

  public void setDateProperties(Map<String, Date> dateProperties) {
    this.dateProperties = dateProperties;
  }

  public Map<String, AudioResource> getAudioResources() {
    return audioResources;
  }

  public void setAudioResources(Map<String, AudioResource> audioResources) {
    this.audioResources = audioResources;
  }

  public Map<String, ImageResource> getImageResources() {
    return imageResources;
  }

  public void setImageResources(Map<String, ImageResource> imageResources) {
    this.imageResources = imageResources;
  }

  public Map<String, VideoResource> getVideoResources() {
    return videoResources;
  }

  public void setVideoResources(Map<String, VideoResource> videoResources) {
    this.videoResources = videoResources;
  }
  
  
}
