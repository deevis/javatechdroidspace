package org.javatech.android.model;

import java.io.Serializable;

public abstract class URIResource implements Serializable {

  private String uri;
  private String name;
  private int id;
  
  public String getUri() {
    return uri;
  }
  public void setUri(String uri) {
    this.uri = uri;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  
  
  
}
