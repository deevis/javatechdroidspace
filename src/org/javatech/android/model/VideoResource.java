package org.javatech.android.model;

import java.io.Serializable;



/**
 * 
 *  
 *  Required settings in AndroidManifest.xml
 
    <uses-sdk android:minSdkVersion="8" />

    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.RECORD_VIDEO" />
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

 * 
 * @author darren
 *
 */
public class VideoResource extends URIResource implements Serializable {

}
