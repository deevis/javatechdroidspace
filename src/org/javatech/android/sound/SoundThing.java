package org.javatech.android.sound;

import android.media.AudioTrack;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.FloatMath;
import android.util.Log;

import java.util.Random;

import org.javatech.android.JT;


public class SoundThing implements Runnable {

	
	public int sampleLength = 4800;
	public int rate = 44100;

	private double freq = 440;
	private double freq2 = 0;

	private double volume = 0.0;
	private boolean go = false;
	private boolean finished = false;

	AudioTrack audioTrack;

	public void run() {
	
		// set up audio Track and start it
		sampleLength = AudioTrack.getMinBufferSize(rate, AudioFormat.CHANNEL_OUT_STEREO , AudioFormat.ENCODING_PCM_16BIT );
		Log.d(JT.APP_NAME, "MinSize = " + sampleLength);
		audioTrack = new AudioTrack( AudioManager.STREAM_MUSIC, rate,AudioFormat.CHANNEL_OUT_STEREO, 
				AudioFormat.ENCODING_PCM_16BIT,sampleLength, AudioTrack.MODE_STREAM);
		Log.d(JT.APP_NAME, "Created audio track");
		audioTrack.play();

		short samples[] = new short[sampleLength];
	
		double increment = (double)((2*Math.PI) * freq / (double)rate); // angular increment for each sample
        double angle = 0;
        
		double increment2 = (double)((2*Math.PI) * freq2 / (double)rate); // angular increment for each sample
        double angle2 = 0;

        double drate = (double)rate;
        
        
		// feed it samples
		while(!finished) {
			if (go) {
				increment = ((2d*Math.PI) * freq / drate);
				increment2 = ((2d*Math.PI) * freq2 / drate);
				double decibels = Math.min( 30 - volume, 30);
				decibels = Math.max( 0, decibels);
				double amplification = 1.0d / Math.pow(2, decibels);
				double amplification2 = 1.0d / Math.pow(2, decibels);
				if ( freq < 7000 ) {
					amplification *= ((80d/freq) + 1d);  
				}
				if ( freq2 < 7000 ) {
					amplification2 *= ((80d/freq2) + 1d);  
				}

				for( int i = 0; i < samples.length; i++ )
			    {
					angle+=increment;
					if (!go) break;
					if ( freq2 <= 0 ) {
						samples[i] = (short)(( Short.MAX_VALUE*( amplification * Math.sin(angle))));  // (( 80/freq) + 1d)  low frequencies need to be amplified!!!   why - i don't know?!?!
					} else {
						angle2+=increment2;
						samples[i] = (short)(( 32767*( amplification * Math.sin(angle) * .5d))  // (( 80/freq) + 1d)  low frequencies need to be amplified!!!   why - i don't know?!?!
							+ ( 32767*( amplification2 * Math.sin(angle2) * .5d)));
					}
			    }	
			    audioTrack.write(samples, 0, sampleLength);


			    //angle%=(2*Math.PI);
			} else {
				angle = 0;
				angle2 = 0;
			}
		}
	}

	public synchronized void finish() {
		this.finished = true;
	}
	
	public synchronized void frequency(double fr) {
		freq = fr;
	}

	public synchronized void frequency2(double fr) {
		freq2 = fr;
	}

	// volume is 0.0 to 1.0
	public synchronized void volume(double vol) {
		volume = vol;
	}

	public void start(boolean g) {
		go = g;
	}

}

