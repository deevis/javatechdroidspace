package org.javatech.android.task;


import ch.boye.httpclientandroidlib.client.methods.HttpDelete;
import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import android.util.Log;

public class HttpDeleteTask extends HttpTask {

  public HttpDeleteTask(String url) {
    super(url);
  }

  @Override
  protected String doInBackground(Object... args) {
    Log.d("HttpDeleteTask", mUrl);
    try {
      HttpDelete get = new HttpDelete(mUrl);
      addHeaders(get);
      return makeRequest(get);
    } catch (Exception e) {
      Log.e("org.javatech.android.HttpPostTask", "Error calling request", e);
      this.exception = e;
      return null;
    }
  }

}
