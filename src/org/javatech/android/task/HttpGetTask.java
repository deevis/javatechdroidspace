package org.javatech.android.task;


import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import android.util.Log;

public class HttpGetTask extends HttpTask {

  public HttpGetTask(String url) {
    super(url);
  }

  @Override
  protected String doInBackground(Object... args) {
    Log.d("HttpGetTask", mUrl);
    try {
      HttpGet get = new HttpGet(mUrl);
      addHeaders(get);
      return makeRequest(get);
    } catch (Exception e) {
      Log.e("org.javatech.android.HttpPostTask", "Error calling request", e);
      this.exception = e;
      return null;
    }
  }

}
