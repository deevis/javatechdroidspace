package org.javatech.android.task;

import java.util.List;

import org.javatech.android.JT;


import ch.boye.httpclientandroidlib.NameValuePair;
import ch.boye.httpclientandroidlib.client.entity.UrlEncodedFormEntity;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;

import android.util.Log;

public class HttpPostTask extends HttpTask {

  protected List<NameValuePair> mPostParams = null;
  
  public HttpPostTask(String url, List<NameValuePair> postParams) {
    super(url);
    mPostParams = postParams;
  }

  @Override
  protected String doInBackground(Object... args) {
    Log.d("HttpPostTask", mUrl + " : " + mPostParams);
    try {
      HttpPost post = new HttpPost(mUrl);
      if ( mPostParams != null ) {
    	  Log.d(JT.APP_NAME, "Adding post parameters" );
    	  post.setEntity( new UrlEncodedFormEntity( mPostParams ));
      }
      addHeaders(post);
      doConfigurePost(post, mPostParams);
      return makeRequest(post);
    } catch (Exception e) {
      Log.e("org.javatech.android.HttpPostTask", "Error calling request", e);
      this.exception = e;
      return null;
    }
  }

	protected void doConfigurePost(HttpPost post, List<NameValuePair> postParams) {
		
	}
  
}
