package org.javatech.android.task;


import java.util.List;

import org.javatech.android.JT;

import ch.boye.httpclientandroidlib.NameValuePair;
import ch.boye.httpclientandroidlib.client.entity.UrlEncodedFormEntity;
import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.client.methods.HttpPut;
import android.util.Log;

public class HttpPutTask extends HttpTask {

  protected List<NameValuePair> mPostParams = null;
  
  public HttpPutTask(String url, List<NameValuePair> postParams) {
    super(url);
    mPostParams = postParams;
  }

  @Override
  protected String doInBackground(Object... args) {
    Log.d("HttpPostTask", mUrl + " : " + mPostParams);
    try {
      HttpPut put = new HttpPut(mUrl);
      if ( mPostParams != null ) {
    	  Log.d(JT.APP_NAME, "Adding post parameters" );
    	  put.setEntity( new UrlEncodedFormEntity( mPostParams ));
      }
      addHeaders(put);
      doConfigurePost(put, mPostParams);
      return makeRequest(put);
    } catch (Exception e) {
      Log.e("org.javatech.android.HttpPostTask", "Error calling request", e);
      this.exception = e;
      return null;
    }
  }

	protected void doConfigurePost(HttpPut post, List<NameValuePair> postParams) {
		
	}

}
