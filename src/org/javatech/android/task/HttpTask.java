package org.javatech.android.task;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.javatech.android.JT;


import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.ClientProtocolException;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpRequestBase;
import ch.boye.httpclientandroidlib.impl.client.BasicResponseHandler;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import ch.boye.httpclientandroidlib.message.AbstractHttpMessage;

import android.os.AsyncTask;
import android.util.Log;

/**
 * HttpClient is going to be ditched by Android.  http://android-developers.blogspot.com/2011/09/androids-http-clients.html
 * 
 * Using a homespun version for Multipart submissions and to have the most current version of HttpClient
 * 
 * @author darren
 *
 */
public abstract class HttpTask extends AsyncTask<Object, Void, String> {

  public HttpTask(String url) {
    mClient = new DefaultHttpClient();
    mUrl = url;
  }
  
  protected String mUrl = null;
  protected HttpClient mClient;
  protected Exception exception;

 

  public Exception getException() {
    return exception;
  }

  public String getContentType() {
		return null;
  }

  
  public Map<String,String> getHeadersMap() {
	  return new HashMap<String,String>();
  }
  
  protected final void addHeaders(AbstractHttpMessage msg) {
	  Map<String, String> headersMap = getHeadersMap();
	  for ( Entry<String,String> e : headersMap.entrySet() ) {
		  msg.addHeader(e.getKey(), e.getValue());
		  Log.d(JT.APP_NAME, "addHeader " + e.getKey() + ":" + e.getValue());
	  }
	  if  ( getContentType() != null ) {
		  Log.d(JT.APP_NAME, "Setting Content-Type : " + getContentType());
		  msg.addHeader("Content-Type", getContentType());
	  }
  }
  
  protected final String makeRequest(HttpRequestBase post) throws ClientProtocolException, IOException {
    HttpResponse httpResponse = mClient.execute(post);
    BasicResponseHandler handler = new BasicResponseHandler();
    String response = handler.handleResponse(httpResponse);
    Log.d("HttpTask", "Returned: " + response);
    return response;
  }

}
