package org.javatech.android.task;

import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.javatech.android.JT;
import org.javatech.android.R;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

public class ImageFetcherTask extends AsyncTask<HttpUriRequest, Void, Drawable> {

  
  protected Context mContext;
  private HttpClient mClient;
  private TaskCallback<Drawable> mCallback = null;
  private static int simultaneousRequests = 0;
  
  public static int MAX_ENTRIES = 100;
  private static Map<String,SoftReference<Drawable>> cache = null;

  public static final Drawable NO_RESULT = new ColorDrawable();
  
  private int maxWidthOrHeight = 100;         // If greater than this...
  private int resizeToWidthOrHeight = 60;     // then resize to this....
  
  static {
    // Create cache
//    cache = (Map<String,Drawable>)Collections.synchronizedMap(
//        new LinkedHashMap<String,Drawable>(MAX_ENTRIES+1, .75F, true) {
//        // This method is called just after a new entry has been added
//        public boolean removeEldestEntry(Map.Entry<String,Drawable> eldest) {
//            return size() > MAX_ENTRIES;
//        }
//    });
	    cache = new LinkedHashMap<String,SoftReference<Drawable>>(MAX_ENTRIES+1, .75F, true) {
	            // This method is called just after a new entry has been added
	            public boolean removeEldestEntry(Map.Entry<String,SoftReference<Drawable>> eldest) {
	                return size() > MAX_ENTRIES;
	            }
	        };
  }
  
  public ImageFetcherTask(Context context, TaskCallback<Drawable> callback) {
    this( context, callback, new DefaultHttpClient());
  }
  
  public ImageFetcherTask(Context context, TaskCallback<Drawable> callback, HttpClient client) {
    mContext = context;
    mClient = client;
    mCallback = callback;
  }
  
  public void setResizeParams(int maxWidthOrHeight, int resizeToWidthOrHeight) {
    this.maxWidthOrHeight = maxWidthOrHeight;
    this.resizeToWidthOrHeight = resizeToWidthOrHeight;
  }
  
  
  public static Drawable lookupDrawable(String url) {
    SoftReference<Drawable> sr = cache.get(url);
    if ( sr != null ) 
    	return sr.get();
    return null;
  }
  
  
  @Override
  protected Drawable doInBackground(HttpUriRequest... args) {
    HttpUriRequest request = args[0];
    try {
      Drawable d = lookupDrawable(request.getURI().toString());
      if ( d != null ) 
        return d;
      
      simultaneousRequests++;
//      Log.d(JT.APP_NAME, "ImageFetcherTask.simultaneousRequests=" + simultaneousRequests);
      Log.d(JT.APP_NAME, "Fetching: " + request.getURI());
      HttpResponse httpResponse = mClient.execute(request);
      InputStream stream = httpResponse.getEntity().getContent();
      d = Drawable.createFromStream(stream, mClient.toString());
      
      Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
      if ( bitmap.getWidth() > maxWidthOrHeight || bitmap.getHeight() > maxWidthOrHeight ) {
        Log.d(JT.APP_NAME, "Resizing from " + bitmap.getWidth() + "," + bitmap.getHeight() + "  to   " + resizeToWidthOrHeight );
    	d = new BitmapDrawable(bitmap.createScaledBitmap(bitmap, resizeToWidthOrHeight, resizeToWidthOrHeight, false));
        bitmap.recycle();
        bitmap = null;		// eager gc, perhaps
      }
//    Log.d(JT.APP_NAME, "[" + bitmap.getWidth() + "," + bitmap.getWidth() + "]");
      cache.put(request.getURI().toString(), new SoftReference(d));
      stream.close();
      return d;
    } catch (UnknownHostException e) {
      Log.d("org.javatech.android.task.ImageFetcherTask", "Error calling request " + request.toString(), e);
      cache.put(request.getURI().toString(), new SoftReference(NO_RESULT));
      return NO_RESULT;
    } catch (Exception e) {
      Log.d("org.javatech.android.task.ImageFetcherTask", "Error calling request " + request.toString(), e);
      cache.put(request.getURI().toString(), new SoftReference(NO_RESULT));
      return NO_RESULT;
    } finally {
      simultaneousRequests--;
//      Log.d(JT.APP_NAME, "ImageFetcherTask.simultaneousRequests=" + simultaneousRequests);
    }
  }

  @Override
  protected void onPostExecute(Drawable result) {
    mCallback.run(result);
  }

}