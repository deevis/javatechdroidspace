package org.javatech.android.task;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.curiouscreature.android.shelves.drawable.FastBitmapDrawable;
import org.curiouscreature.android.shelves.util.ImageUtilities;
import org.curiouscreature.android.shelves.util.ImageUtilities.ExpiringBitmap;
import org.curiouscreature.android.shelves.util.UserTask;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class ImageFetcherUserTask extends UserTask<HttpUriRequest, Void, Drawable> {

  
  protected Context mContext;
  private HttpClient mClient;
  private TaskCallback<Drawable> mCallback = null;
  public static FastBitmapDrawable NULL_PLACEHOLDER = new FastBitmapDrawable(null);
  
  private int maxWidthOrHeight = 100;         // If greater than this...
  private int resizeToWidthOrHeight = 60;     // then resize to this....
  
  
  public ImageFetcherUserTask(Context context, TaskCallback<Drawable> callback) {
    this( context, callback, new DefaultHttpClient());
  }
  
  public ImageFetcherUserTask(Context context, TaskCallback<Drawable> callback, HttpClient client) {
    mContext = context;
    mClient = client;
    mCallback = callback;
  }
  
  public void setResizeParams(int maxWidthOrHeight, int resizeToWidthOrHeight) {
    this.maxWidthOrHeight = maxWidthOrHeight;
    this.resizeToWidthOrHeight = resizeToWidthOrHeight;
  }
  
  
  
  @Override
  public Drawable doInBackground(HttpUriRequest... args) {
	  HttpUriRequest request = args[0];
	  String url = request.getURI().toString();
	  ExpiringBitmap bm = ImageUtilities.load(url);
	  FastBitmapDrawable drawable = ImageUtilities.addUrlCache(mContext, url, bm.bitmap);
	  return drawable;
  }

  @Override
  public void onPostExecute(Drawable result) {
    mCallback.run(result);
  }

}