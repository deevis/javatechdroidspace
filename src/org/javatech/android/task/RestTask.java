package org.javatech.android.task;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.javatech.android.JT;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class RestTask extends AsyncTask<HttpUriRequest, Void, String> {

  public static final String HTTP_REQUEST = "httpRequest";
  public static final String HTTP_RESPONSE = "httpResponse";
  public static final String SENDING_CLASS = "sendingClass";
  
  protected Context mContext;
  protected HttpClient mClient;
  protected String mAction;
  protected String mRequest;
  protected Runnable callback;
  
  public RestTask(Context context, String action) {
    this( context, action, new DefaultHttpClient());
  }
  

  public RestTask(Context context, String action, Runnable callback) {
    this( context, action, new DefaultHttpClient());
    this.callback = callback;
  }

  public RestTask(Context context, String action, HttpClient client) {
    mContext = context;
    mAction = action;
    mClient = client;
    Log.d(JT.APP_NAME, "RestTask() : " + action);
  }
  
  @Override
  protected String doInBackground(HttpUriRequest... args) {
    HttpUriRequest request = args[0];
    mRequest = request.getURI().toString();
    Log.d("RestTask", mRequest);
    try {
      HttpResponse httpResponse = mClient.execute(request);
      BasicResponseHandler handler = new BasicResponseHandler();
      String response = handler.handleResponse(httpResponse);
      return response;
    } catch (Exception e) {
      Log.d("org.javatech.android.RestTask", "Error calling request " + request.getURI().toString(), e);
      return null;
    }
  }

  @Override
  protected void onPostExecute(String result) {
    if ( callback != null ) {
      callback.run();
    }
    Intent i = new Intent(mAction);
    Log.d(JT.APP_NAME, "RestTaskonPostExecute() : " + mAction);
    i.putExtra(HTTP_REQUEST, mRequest);
    i.putExtra(HTTP_RESPONSE, result);
    i.putExtra(SENDING_CLASS, this.getClass().getName());
    mContext.sendBroadcast(i);
  }

  
}
