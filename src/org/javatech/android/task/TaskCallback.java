package org.javatech.android.task;

public interface TaskCallback<T extends Object> {

  public void run(T t);
  
}
