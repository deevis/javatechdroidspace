package org.javatech.android.task;

import java.util.Stack;

import android.os.AsyncTask;

public class TaskStack {

  private Stack<AsyncTask> stack = new Stack<AsyncTask>();
  
  private static TaskStack _instance = new TaskStack();
  
  private TaskStack() {}
  
  public static void push(AsyncTask task) {
    _instance.stack.push(task);
  }
  
  public void run() {
//    while ( running ) {
//      
//    }
  }
}
