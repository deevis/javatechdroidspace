package org.javatech.android.task;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.javatech.android.JT;

import android.os.AsyncTask;
import android.util.Log;

public class UrlMetaRetrievalTask extends AsyncTask<String, String[], String[]> {

  
  public UrlMetaRetrievalTask() {
    super();
  }

  @Override
  protected String[] doInBackground(String... urls) {
    String[] actualUrls = new String[urls.length];
    DefaultHttpClient client = new DefaultHttpClient();
    for ( int i = 0; i < urls.length; i++ ) {
      String url = urls[i];
      actualUrls[i] = url;
      Log.d(JT.APP_NAME, "Getting MetaInfo for: " + url);
      HttpUriRequest get = new HttpHead(url);
      try {
        
        HttpResponse response = client.execute(get);
        Header[] allHeaders = response.getAllHeaders();
        for ( Header h : allHeaders ) {
          Log.d(JT.APP_NAME, h.getName() + " [" + h.getValue() + "]");
        }
        Header locHeader = response.getFirstHeader("Location");
        if ( locHeader != null ) {
          actualUrls[i] = locHeader.getValue();
        }
        Log.d(JT.APP_NAME, "Actual Location: " + actualUrls[i]);
      } catch ( Exception e ) {
        e.printStackTrace();
        continue;
      }
    }
    return actualUrls;
  }

}
