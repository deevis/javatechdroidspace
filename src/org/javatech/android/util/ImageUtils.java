package org.javatech.android.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

import org.javatech.android.JT;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

public class ImageUtils {

	public static void addBitmap(Context c, String fileName, int width, Bitmap bm, Map<String,String> imageData) throws FileNotFoundException {
	    Bitmap scaledBitmap = Bitmap.createScaledBitmap(bm, width, width, false);
	    String filePath = getTempImagePath(c, fileName);
	    Log.d(JT.APP_NAME, "Writing new item image [" + fileName + "] to " + filePath);
	    scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 70, new FileOutputStream(new File( filePath )));
	    scaledBitmap.recycle();
		imageData.put(fileName, filePath);
	}

	  public static String getTempImagePath(Context c, String fileName) {
		  //String cacheDir = this.getCacheDir().getAbsolutePath();
		  return JT.getSdCardStorage(c) + "/files/" + fileName + ".jpg";
		  //return cacheDir + "myItems/" + fileName + ".jpg";  
	  }


}
