package org.javatech.android.xml;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;

import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;


/**
 * TODO: parse this: http://feeds.feedburner.com/ImgurGallery?format=xml
 * 
 * http://feed.photobucket.com/images/guinea%20pig/feed.rss
 * 
 * @author darren
 *
 */
public class AndroidSaxFeedParser extends BaseFeedParser {

  public AndroidSaxFeedParser(URL feedUrl) {
      super(feedUrl);
  }

  public AndroidSaxFeedParser(String xmlContent) {
    super(xmlContent);
}

  public List<Message> parse() {
      final Message currentMessage = new Message();
      RootElement root = new RootElement("rss");
      final List<Message> messages = new ArrayList<Message>();
      Element channel = root.getChild("channel");
      Element item = channel.getChild(ITEM);
      item.setEndElementListener(new EndElementListener(){
          public void end() {
              messages.add(currentMessage.copy());
          }
      });
      item.getChild(TITLE).setEndTextElementListener(new EndTextElementListener(){
          public void end(String body) {
              currentMessage.setTitle(body);
          }
      });
      item.getChild(GUID).setEndTextElementListener(new EndTextElementListener(){
        public void end(String body) {
            currentMessage.setGuid(body);
        }
    });
      item.getChild(LINK).setEndTextElementListener(new EndTextElementListener(){
          public void end(String body) {
              currentMessage.setLink(body);
          }
      });
      item.getChild(DESCRIPTION).setEndTextElementListener(new EndTextElementListener(){
          public void end(String body) {
              currentMessage.setDescription(body);
          }
      });
      item.getChild(PUB_DATE).setEndTextElementListener(new EndTextElementListener(){
        public void end(String body) {
            currentMessage.setDate(body);
        }
    });
      item.getChild(NS_DC, CREATOR).setEndTextElementListener(new EndTextElementListener(){
          public void end(String body) {
              currentMessage.setCreator(body);
          }
      });
      Element mediaContent = item.getChild(NS_MEDIA, CONTENT);
      mediaContent.setStartElementListener(new StartElementListener() {
        @Override
        public void start(Attributes attributes) {
          currentMessage.setContentUrl( attributes.getValue("url"));
        }
      });
      mediaContent.getChild(NS_MEDIA, THUMBNAIL).setStartElementListener(new StartElementListener() {
        @Override
        public void start(Attributes attributes) {
          currentMessage.setThumbUrl( attributes.getValue("url"));
        }
      });
      try {
          Xml.parse(this.getInputStream(), Xml.Encoding.UTF_8, root.getContentHandler());
      } catch (Exception e) {
          throw new RuntimeException(e);
      }
      return messages;
  }
}