package org.javatech.android.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public abstract class BaseFeedParser implements FeedParser {

  // names of the XML tags
  static final String PUB_DATE = "pubDate";
  static final  String DESCRIPTION = "description";
  static final  String GUID = "guid";
  static final  String LINK = "link";
  static final  String TITLE = "title";
  static final  String ITEM = "item";
  static final  String NS_MEDIA = "http://search.yahoo.com/mrss/";
  static final  String NS_DC = "http://purl.org/dc/elements/1.1/";
  static final  String CREATOR = "creator";
  static final  String CONTENT = "content";
  static final  String THUMBNAIL = "thumbnail";

  
  final URL feedUrl;
  final String xmlContent;
  
  protected BaseFeedParser(URL feedUrl){
    this.feedUrl = feedUrl;
    this.xmlContent = null;
  }

  protected BaseFeedParser(String xmlContent) {
    this.feedUrl = null;
    this.xmlContent = xmlContent;
  }
  
  protected InputStream getInputStream() {
      try {
          if ( feedUrl != null ) {
            return feedUrl.openConnection().getInputStream();
          } else if ( xmlContent != null ) {
            return new ByteArrayInputStream(xmlContent.getBytes());
          } else {
            return null;
          }
      } catch (IOException e) {
          throw new RuntimeException(e);
      }
  }
}
