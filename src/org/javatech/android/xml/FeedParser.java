package org.javatech.android.xml;

import java.util.List;

public interface FeedParser {
  List<Message> parse();
}
